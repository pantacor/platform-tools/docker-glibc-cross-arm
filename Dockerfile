FROM debian:stretch
MAINTAINER Alexander Sack <asac129@gmail.com>

# Install build tools
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -yy && \
    DEBIAN_FRONTEND=noninteractive apt-get install -yy \
        automake            \
        bison               \
        build-essential     \
        curl                \
        file                \
        flex                \
        git                 \
        libtool             \
        pkg-config          \
        python              \
        texinfo             \
        vim                 \
        wget

# Install linaro libc cross armhf
RUN mkdir -p /build/arm-linux-gnueabihf /opt/cross/arm-linux-gnueabihf/ && \
    cd /build && \
    wget -qO gcc-linaro.tar.xz https://releases.linaro.org/components/toolchain/binaries/6.3-2017.05/arm-linux-gnueabihf/gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf.tar.xz && \
    cd arm-linux-gnueabihf && \
    tar --strip 1 -xf ../gcc-linaro.tar.xz && \
    find && \
    rm -rf ../gcc-linaro.tar.xz && \
    cd / && \
    cp -rf /build/* /opt/cross/ && \
    rm -rf /build/*


# Install linaro libc cross armhf
RUN mkdir -p /build/arm-linux-gnueabi /opt/cross/arm-linux-gnueabi/ && \
    cd /build && \
    wget -qO gcc-linaro.tar.xz https://releases.linaro.org/components/toolchain/binaries/6.3-2017.05/arm-linux-gnueabi/gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabi.tar.xz && \
    cd arm-linux-gnueabi && \
    tar --strip 1 -xf ../gcc-linaro.tar.xz && \
    find && \
    rm -rf ../gcc-linaro.tar.xz && \
    cd / && \
    cp -rf /build/* /opt/cross/ && \
    rm -rf /build/*


ENV PATH $PATH:/opt/cross/arm-linux-gnueabi/bin:/opt/cross/arm-linux-gnueabihf/bin/
CMD /bin/bash
